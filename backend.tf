terraform {
  backend "s3" {
    bucket = "tf-state-mostagir"
    key    = "terraform.tfstate"
    region = "us-east-1"
  }
}
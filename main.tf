resource "aws_security_group" "gitlab_sg" {
  description = "Allow HTTP traffic"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  tags = var.common_tags
}

resource "aws_instance" "gitlab" {
  ami           = var.ubuntu_ami
  instance_type = "t3a.large"

  key_name = var.key_pair
  vpc_security_group_ids = [aws_security_group.gitlab_sg.id]

  ebs_block_device {
    device_name = "/dev/sda1"
    volume_size = 50
    volume_type = "gp3"
    delete_on_termination = true
  }

  associate_public_ip_address = true

  tags = var.common_tags

}

resource "random_password" "gitlab_root_password" {
  length           = 16
  special          = true
  override_special = "_%@"
}

data "template_file" "install_gitlab_script" {
  template = file("${path.module}/scripts/install_gitlab.tpl")

  vars = {
    root_password = random_password.gitlab_root_password.result
    aws_public_ip = aws_instance.gitlab.public_ip
  }
}

resource "null_resource" "install_gitlab" {
  count      = var.install_gitlab ? 1 : 0
  depends_on = [aws_instance.gitlab]

  triggers = {
    instance_id = aws_instance.gitlab.id
  }

  connection {
    type        = "ssh"
    user        = "ubuntu"
    # private_key = file("~/Desktop/awskey.pem")
    private_key = file("${var.SSH_PRIVATE_KEY}")
    host        = aws_instance.gitlab.public_ip
  }

  provisioner "remote-exec" {
    inline = [
      data.template_file.install_gitlab_script.rendered
    ]
  }
}
output "instance_access_link" {
  description = "Access link to the GitLab instance"
  value       = "http://${aws_instance.gitlab.public_ip}"
}

output "gitlab_root_password" {
  value     = random_password.gitlab_root_password.result
  sensitive = true
}
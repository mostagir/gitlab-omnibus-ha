#!/bin/bash
sudo apt-get update
sudo apt-get install -y curl openssh-server ca-certificates tzdata perl
curl -sS https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.deb.sh | sudo bash
sudo GITLAB_ROOT_PASSWORD='${root_password}' EXTERNAL_URL='http://${aws_public_ip}' apt install gitlab-ce
sudo gitlab-ctl reconfigure

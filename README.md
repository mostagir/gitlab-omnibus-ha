# Gitlab Omnibus Terraform

[![pipeline status](https://gitlab.com/mostagir/gitlab-omnibus-ha/badges/main/pipeline.svg)](https://gitlab.com/mostagir/gitlab-omnibus-ha/commits/main)

## Table of Contents

- [Introduction](#introduction)
- [Prerequisites](#prerequisites)
- [Terraform Structure](#terraform-structure)
- [GitLab CI/CD Pipeline](#gitlab-cicd-pipeline)
- [Deployment Process](#deployment-process)
- [Accessing GitLab](#accessing-gitlab)
- [Contributing](#contributing)

## Introduction

The following project will install GitLab on an AWS EC2 instance using Terraform and the GitLab Omnibus package. The project is designed to be deployed using GitLab CI/CD, but can also be deployed manually.

## Prerequisites

Before getting started, you'll need to manually set up some AWS resources and configurations:

1. **IAM User (`gitlab_ci`):** Create an IAM user named `gitlab_ci` with programmatic access. This user will be used by the GitLab Runner to authenticate with AWS and perform necessary actions.

2. **IAM Role (`terraform`):** Create an IAM role named `terraform` with the necessary permissions to create, update, and delete AWS resources. This role should have a trust relationship that allows the `gitlab_ci` user to assume this role.

3. **S3 Bucket:** Create an S3 bucket to store the Terraform state file. This bucket should have versioning enabled to keep track of state history.

4. **IAM Policy:** Attach a policy to the `gitlab_ci` user that allows access to the S3 bucket and the ability to assume the `terraform` role. Similarly, the `terraform` role should have a policy allowing it to perform necessary actions on AWS resources.

5. **AWS CLI:** Ensure that the AWS CLI is installed and configured with the necessary access credentials.

6. **Terraform:** Ensure that Terraform is installed.

Ensure that the `gitlab_ci` user has the necessary permissions to access the S3 bucket and assume the `terraform` role. Similarly, ensure that the `terraform` role has the necessary permissions to manage AWS resources.

## Installation Script

The GitLab installation is automated through a script located in the `scripts/` directory. The script file is named `install_gitlab.tpl`. This script is used by Terraform to set up GitLab on the provisioned EC2 instance. Here's a breakdown of what the script does:

1. **Update Package List:**
    ```bash
    sudo apt-get update
    ```
   The script starts by updating the package list on the EC2 instance to ensure it has the latest information about available packages and their versions.

2. **Install Necessary Packages:**
    ```bash
    sudo apt-get install -y curl openssh-server ca-certificates tzdata perl
    ```
   Installs necessary packages required for the GitLab installation and configuration.

3. **Add GitLab Repository:**
    ```bash
    curl -sS https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.deb.sh | sudo bash
    ```
   Adds the GitLab repository to the list of sources, so that the `gitlab-ce` package can be found and installed.

4. **Install GitLab:**
    ```bash
    sudo GITLAB_ROOT_PASSWORD='${root_password}' EXTERNAL_URL='http://${aws_public_ip}' apt install gitlab-ce
    ```
   Installs GitLab Community Edition. It also sets the root password for GitLab and the external URL to the public IP of the EC2 instance. These values are passed in as variables from Terraform.

5. **Reconfigure GitLab:**
    ```bash
    sudo gitlab-ctl reconfigure
    ```
   Runs the `gitlab-ctl reconfigure` command to ensure GitLab is properly configured.

This script is utilized within the Terraform configuration in the `remote-exec` provisioner block. It's rendered with the necessary variables and executed on the EC2 instance to automate the installation and configuration of GitLab.

## Terraform Structure

This project uses Terraform to provision and manage AWS resources. The Terraform configuration is organized into several files for better modularity and easier management:

- **main.tf:** This file contains the core resource definitions and configurations for your AWS infrastructure.

- **variables.tf:** This file defines the input variables that your Terraform configuration will use.

- **provider.tf:** This file configures the AWS provider for Terraform.

- **backend.tf:** This file configures the backend for Terraform, specifying the S3 bucket to store the state file.

- **terraform.tfvars:** This file provides values for the input variables defined in `variables.tf`.

- **versions.tf:** This file specifies the required version of Terraform and the AWS provider.

## GitLab CI/CD Pipeline

The GitLab CI/CD pipeline is defined in the `.gitlab-ci.yml` file and is organized into several stages to ensure a controlled and reviewable deployment process:

### before_script

This section is executed before each job and sets up the environment by exporting necessary AWS credentials, installing dependencies, and initializing Terraform.

### plan_infra

In this stage, Terraform performs a plan operation to generate an execution plan for the infrastructure setup, excluding the GitLab installation. The plan is saved as an artifact for later use.

### apply_infra

This manual stage applies the infrastructure plan generated in the previous stage, setting up the necessary AWS resources.

### plan_gitlab

This stage re-plans the Terraform configuration, this time including the GitLab installation. The plan is saved as an artifact for later use.

### apply_gitlab

This manual stage applies the GitLab installation plan generated in the previous stage, installing GitLab on the provisioned infrastructure.

### output_password

This manual stage outputs the generated GitLab root user password for access.

## Deployment Process

1. Ensure that all prerequisites are met.
2. Run the `plan_infra` stage of the pipeline, which will automatically execute.
3. Review the plan, and if it looks good, manually trigger the `apply_infra` stage.
4. Once the infrastructure is set up, manually trigger the `apply_gitlab` stage to install GitLab.
5. After GitLab is installed, manually trigger the `output_password` stage to retrieve the GitLab root user password.

## Accessing GitLab

Once the deployment process is complete, navigate to the provisioned AWS instance's public IP address using a web browser. Log in to GitLab using the username `root` and the password outputted in the `output_password` stage of the pipeline.

## Environment Variables

- **AWS_ACCESS_KEY_ID:** This environment variable is used to specify the AWS access key ID for the `gitlab_ci` IAM user. It is used by the AWS CLI and Terraform to authenticate with AWS and perform necessary actions.

- **AWS_SECRET_ACCESS_KEY:** This environment variable is used to specify the AWS secret access key for the `gitlab_ci` IAM user. It is used in conjunction with `AWS_ACCESS_KEY_ID` to authenticate with AWS.

- **SSH_PRIVATE_KEY:** This environment variable is used to specify the path to the SSH private key file. This private key is used by Terraform to securely connect to the provisioned EC2 instances for configuration.

Ensure to securely manage these environment variables and not expose them publicly. They grant significant permissions to your AWS account and should be handled with care.

## Additional Notes

- It's crucial to keep your AWS credentials secure to prevent unauthorized access to your AWS account.
- Ensure that the IAM policies attached to `gitlab_ci` user and `terraform` role are appropriately scoped to prevent excessive permissions.
- It's advisable to review the Terraform plans in each stage before applying them to catch any potential issues early.
- Keep your Terraform state file secure, as it can contain sensitive information about your AWS infrastructure.

## Contributing

For contributing fork the project and create a merge request.

## License

MIT License 2.0



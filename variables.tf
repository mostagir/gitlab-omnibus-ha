variable "aws_region" {
  description = "The AWS region where resources will be created."
  default     = "us-east-1"  # You can set a default value or remove this line to require an explicit value
  type        = string
}

variable "terraform_role_arn" {
  description = "The ARN of the IAM role to be assumed by Terraform."
  type        = string
}

variable "key_pair" {
  description = "Name of the key pair used to SSH to EC2 instances."
}

variable "common_tags" {
  description = "Tags to be applied to all resources."
  type        = map(string)
}

variable "ubuntu_ami" {
  description = "AMI ID for Ubuntu instance in the given region"
  default     = "ami-053b0d53c279acc90"
}

variable "install_gitlab" {
  description = "Flag to control GitLab installation"
  type        = bool
  default     = false
}

variable "SSH_PRIVATE_KEY" {
  description = "The SSH private key to be used for the connection."
  type        = string
  sensitive   = true
}

